package net.wouto.timers;

import net.wouto.timers.handler.AsyncTimerModel;
import net.wouto.timers.handler.SyncTimerModel;
import net.wouto.timers.handler.impl.AsyncThreadPoolTimer;
import net.wouto.timers.handler.impl.SyncThreadPoolTimer;
import net.wouto.timers.handler.impl.ThreadPoolTimer;
import net.wouto.timers.handler.TimerModel;
import org.junit.Assert;

public class Timers {

	/**
	 * Create a new Timer that runs tasks both async and sync.
	 * To run the sync tasks you'll need to invoke {@link TimerModel#tick()} in your own synchronous thread.
	 * @param threads The amount of threads this threadpool should start with
	 * @return A TimerModel instance to execute tasks on
	 */
	public static TimerModel createThreadPoolScheduler(int threads) {
		Assert.assertTrue(threads > 0 && threads < 256);
		return new ThreadPoolTimer(threads);
	}

	/**
	 * Create a new Timer that runs tasks asynchronously
	 * @param threads The amount of threads this threadpool should start with
	 * @return A TimerModel instance to execute tasks on
	 */
	public static AsyncTimerModel createAsyncThreadPoolScheduler(int threads) {
		Assert.assertTrue(threads > 0 && threads < 256);
		return new AsyncThreadPoolTimer(threads);
	}

	/**
	 * Create a new Timer that runs tasks synchronously
	 * To run the sync tasks you'll need to invoke {@link TimerModel#tick()} in your own synchronous thread.
	 * @return A TimerModel instance to execute tasks on
	 */
	public static SyncTimerModel createSyncThreadPoolScheduler() {
		return new SyncThreadPoolTimer();
	}

}
