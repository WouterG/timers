package net.wouto.timers.obj;

public class TimerRunnable implements Runnable {

	private long id;
	private Runnable r;
	private Runnable runAfter;

	public TimerRunnable(long id, Runnable r) {
		this.id = id;
		this.r = r;
	}

	public void setRunAfter(Runnable r) {
		this.runAfter = r;
	}

	public Runnable getRunnable() {
		return this.r;
	}

	@Override
	public void run() {
		try {
			this.r.run();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		if (this.runAfter != null) {
			try {
				this.runAfter.run();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public long getId() {
		return this.id;
	}

}
