package net.wouto.timers.obj;

import net.wouto.timers.handler.BaseTimerModel;
import net.wouto.timers.handler.SyncTimerModel;

public class ScheduledTask {

	private final BaseTimerModel base;
	private final long id;
	private TimerRunnable job;
	private long wait;
	private long interval;
	private boolean delay;
	private boolean repeat;
	private boolean async;

	private boolean cancelled;
	private long scheduleTime;
	private long lastExecutionTime;

	public ScheduledTask(BaseTimerModel base, long id, TimerRunnable r, long wait, long interval, boolean async) {
		this.scheduleTime = System.currentTimeMillis();
		this.lastExecutionTime = -1;
		this.base = base;
		this.id = id;
		this.job = r;
		this.wait = wait;
		this.interval = interval;
		this.delay = this.wait > 0;
		this.repeat = this.interval > 0;
		this.async = async;
		this.cancelled = false;
	}

	public ScheduledTask(SyncTimerModel base, long id, TimerRunnable r, long wait, long interval) {
		this(base, id, r, wait, interval, false);
	}

	public ScheduledTask(SyncTimerModel base, long id, TimerRunnable r, long wait) {
		this(base, id, r, wait, -1);
	}

	public ScheduledTask(SyncTimerModel base, long id, TimerRunnable r) {
		this(base, id, r, -1);
	}

	public BaseTimerModel getBase() {
		return this.base;
	}

	/**
	 * The task id of this job. Use this to cancel/stop the task later on
	 * @return The id
	 */
	public long getId() {
		return this.id;
	}

	public TimerRunnable getJob() {
		return job;
	}

	public long getWait() {
		return wait;
	}

	public long getInterval() {
		return interval;
	}

	public boolean isDelay() {
		return delay;
	}

	public boolean isRepeat() {
		return repeat;
	}

	public boolean isAsync() {
		return async;
	}

	public boolean shouldStart() {
		if (this.lastExecutionTime == -1) {
			if (this.isDelay()) {
				return System.currentTimeMillis() >= this.scheduleTime + this.wait;
			}
		} else if (this.isRepeat()) {
			return System.currentTimeMillis() >= this.lastExecutionTime + this.interval;
		}
		return this.lastExecutionTime == -1;
	}

	public void ran() {
		this.lastExecutionTime = System.currentTimeMillis();
	}

	public void cancel() {
		this.cancelled = true;
		this.base.cancel(this.id);
	}

	public void stop() {
		this.cancel();
		this.base.stop(this.id);
	}

	public boolean isCancelled() {
		return this.cancelled;
	}

}
