package net.wouto.timers.handler;

/**
 * Combines {@link SyncTimerModel} and {@link AsyncTimerModel} into one interface
 */
public interface TimerModel extends SyncTimerModel, AsyncTimerModel {
}
