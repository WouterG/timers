package net.wouto.timers.handler;

import net.wouto.timers.obj.ScheduledTask;

/**
 * Base for timer scheduler classes
 */
public interface BaseTimerModel {

	/**
	 * Stops a Job from running. Asynchronous jobs are halted immediately, Synchronous jobs cannot be interrupted.
	 * See also {@link ScheduledTask#stop()}
	 * @param id The id of the job to interrupt.
	 * @return whether the job was found and stopped
	 */
	boolean stop(long id);

	/**
	 * Prevents further execution of this job, if the job is a repeating or delayed task. Running jobs won't be stopped,
	 * but they won't be invoked again in the future.
	 * See also {@link ScheduledTask#cancel()}
	 * @param id The id of the job to interrupt.
	 * @return whether the job was found and stopped
	 */
	boolean cancel(long id);

}
