package net.wouto.timers.handler;

import net.wouto.timers.obj.ScheduledTask;

/**
 * Model for asynchronous timer schedulers
 */
public interface AsyncTimerModel extends BaseTimerModel {

	/**
	 * Schedule a new job to run as quickly as possible on a separate thread
	 * @param runnable The job to execute
	 * @return a ScheduledTask instance with which you can keep track of the status of the job and cancel/stop it.
	 * @throws Throwable Any errors that occur during the scheduling of this job
	 */
	ScheduledTask runAsync(Runnable runnable) throws Throwable;

	/**
	 * Schedule a new job to run after a certain delay on a separate thread
	 * @param runnable The job to execute
	 * @param wait The amount of milliseconds to wait before executing the job
	 * @return a ScheduledTask instance with which you can keep track of the status of the job and cancel/stop it.
	 * @throws Throwable Any errors that occur during the scheduling of this job
	 */
	ScheduledTask runAsyncLater(Runnable runnable, long wait) throws Throwable;

	/**
	 * Schedule a new job to run periodically after a certain delay and then repeatedly on the interval, on a separate thread
	 * @param runnable The job to execute
	 * @param wait The amount of milliseconds to wait before executing the job
	 * @param interval The amount of milliseconds to wait inbetween executions
	 * @return a ScheduledTask instance with which you can keep track of the status of the job and cancel/stop it.
	 * @throws Throwable Any errors that occur during the scheduling of this job
	 */
	ScheduledTask runAsyncRepeating(Runnable runnable, long wait, long interval) throws Throwable;

}
