package net.wouto.timers.handler;

import net.wouto.timers.obj.ScheduledTask;

/**
 * Model for synchronous timer schedulers
 */
public interface SyncTimerModel extends BaseTimerModel {

	/**
	 * This function has to be invoked to make synchronous tasks execute
	 */
	void tick();

	/**
	 * Schedule a job to run synchronously as soon as possible
	 * <b>You need to invoke {@link SyncTimerModel#tick()} for this to work</b>
	 * @param runnable The job to run
	 * @return a ScheduledTask instance with which you can keep track of the status of the job and cancel/stop it.
	 * @throws Throwable Any errors that occur during the scheduling of this job
	 */
	ScheduledTask runSync(Runnable runnable) throws Throwable;

	/**
	 * Schedule a job to run synchronously after the wait time has passed
	 * <b>You need to invoke {@link SyncTimerModel#tick()} for this to work</b>
	 * @param runnable The job to run
	 * @param wait The amount of milliseconds to wait before running this task
	 * @return a ScheduledTask instance with which you can keep track of the status of the job and cancel/stop it.
	 * @throws Throwable Any errors that occur during the scheduling of this job
	 */
	ScheduledTask runSyncLater(Runnable runnable, long wait) throws Throwable;

	/**
	 * Schedule a job to run periodically on a specified interval, and after a certain amount of time
	 * <b>You need to invoke {@link SyncTimerModel#tick()} for this to work</b>
	 * @param runnable The job to run
	 * @param wait The amount of milliseconds to wait before starting this timer
	 * @param interval The amount of milliseconds to wait in between executions
	 * @return a ScheduledTask instance with which you can keep track of the status of the job and cancel/stop it.
	 * @throws Throwable Any errors that occur during the scheduling of this job
	 */
	ScheduledTask runSyncRepeating(Runnable runnable, long wait, long interval) throws Throwable;

}
