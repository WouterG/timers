package net.wouto.timers.handler.impl;

import net.wouto.timers.handler.AsyncTimerModel;
import net.wouto.timers.obj.ScheduledTask;
import net.wouto.timers.obj.TimerRunnable;
import net.wouto.timers.util.TimerThreadFactory;
import org.junit.Assert;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class AsyncThreadPoolTimer implements AsyncTimerModel {

	private ScheduledExecutorService executor;
	private Map<Long, ScheduledTask> queue = new ConcurrentHashMap<>();
	private Map<ScheduledTask, ScheduledFuture> runningAsyncTasks = new ConcurrentHashMap<>();
	private Map<Long, ScheduledTask> running = new ConcurrentHashMap<>();
	private AtomicLong count;

	public AsyncThreadPoolTimer(int threadCount) {
		this.executor = Executors.newScheduledThreadPool(threadCount, new TimerThreadFactory(this.getClass()));

		this.count = new AtomicLong();
	}

	private ScheduledTask createScheduledTask(Runnable r, long wait, long interval, boolean async) {
		List<ScheduledTask> rem = new ArrayList<>();
		for (Map.Entry<ScheduledTask, ScheduledFuture> e : runningAsyncTasks.entrySet()) {
			if (e.getValue().isCancelled() && e.getValue().isDone()) {
				rem.add(e.getKey());
			}
		}
		rem.forEach((e) -> {
			runningAsyncTasks.remove(e);
		});
		long tid = count.getAndIncrement();
		TimerRunnable timerRunnable = new TimerRunnable(tid, r);
		ScheduledTask t = new ScheduledTask(
				this, tid, timerRunnable, wait, interval, async
		);
		this.queue.put(tid, t);
		timerRunnable.setRunAfter(() -> {
			running.put(t.getId(), t);
			this.queue.remove(tid);
		});
		ScheduledFuture sf;
		if (t.getInterval() > 0) {
			sf = this.executor.scheduleAtFixedRate(t.getJob(), t.getWait(), t.getInterval(), TimeUnit.MILLISECONDS);
			runningAsyncTasks.put(t, sf);
		} else {
			sf = this.executor.schedule(t.getJob(), t.getWait(), TimeUnit.MILLISECONDS);
			runningAsyncTasks.put(t, sf);
		}
		return t;
	}

	@Override
	public boolean stop(long id) {
		ScheduledTask task = this.running.remove(id);
		if (task == null) {
			return false;
		}
		ScheduledFuture future = this.runningAsyncTasks.remove(task);
		future.cancel(true);
		return true;
	}

	@Override
	public boolean cancel(long id) {
		ScheduledTask task = this.running.remove(id);
		if (task == null) {
			return false;
		}
		ScheduledFuture future = this.runningAsyncTasks.get(task);
		future.cancel(false);
		return true;
	}

	@Override
	public ScheduledTask runAsync(Runnable runnable) throws Throwable {
		Assert.assertNotNull(runnable);
		return createScheduledTask(runnable, -1, -1, true);
	}

	@Override
	public ScheduledTask runAsyncLater(Runnable runnable, long wait) throws Throwable {
		Assert.assertNotNull(runnable);
		Assert.assertTrue(wait > 0);
		return createScheduledTask(runnable, wait, -1, true);
	}

	@Override
	public ScheduledTask runAsyncRepeating(Runnable runnable, long wait, long interval) throws Throwable {
		Assert.assertNotNull(runnable);
		Assert.assertTrue(wait > 0 && interval > 0);
		return createScheduledTask(runnable, wait, interval, true);
	}
}
