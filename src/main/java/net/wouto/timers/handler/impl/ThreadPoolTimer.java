package net.wouto.timers.handler.impl;

import net.wouto.timers.handler.TimerModel;
import net.wouto.timers.obj.ScheduledTask;
import net.wouto.timers.obj.TimerRunnable;
import net.wouto.timers.util.TimerThreadFactory;
import org.junit.Assert;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class ThreadPoolTimer implements TimerModel {

	private ScheduledExecutorService executor;
	private Map<Long, ScheduledTask> queue = new ConcurrentHashMap<>();
	private Map<ScheduledTask, ScheduledFuture> runningAsyncTasks = new ConcurrentHashMap<>();
	private Map<Long, ScheduledTask> running = new ConcurrentHashMap<>();
	private AtomicLong count;

	public ThreadPoolTimer(int threadCount) {
		this.executor = Executors.newScheduledThreadPool(threadCount, new TimerThreadFactory(this.getClass()));

		this.count = new AtomicLong();
	}

	@Override
	public void tick() {
		List<ScheduledTask> rem = new ArrayList<>();
		for (Map.Entry<ScheduledTask, ScheduledFuture> e : runningAsyncTasks.entrySet()) {
			if (e.getValue().isCancelled() && e.getValue().isDone()) {
				rem.add(e.getKey());
			}
		}
		rem.forEach((e) -> {
			runningAsyncTasks.remove(e);
		});
		List<Long> started = new ArrayList();
		this.queue.values().forEach((e) -> {
			if (e.shouldStart()) {
				if (e.isCancelled()) {
					started.add(e.getId());
					return;
				}
				if (e.isAsync()) {
					ScheduledFuture sf;
					if (e.getInterval() > 0) {
						sf = this.executor.scheduleAtFixedRate(e.getJob(), 0L, e.getInterval(), TimeUnit.MILLISECONDS);
					} else {
						sf = this.executor.schedule(e.getJob(), 0L, TimeUnit.MILLISECONDS);
					}
					this.running.put(e.getId(), e);
					this.runningAsyncTasks.put(e, sf);
				} else {
					if (!running.containsKey(e.getId())) {
						running.put(e.getId(), e);
					}
					e.getJob().run();
					e.ran();
					if (!e.isRepeat() || e.isCancelled()) {
						running.remove(e.getId());
					}
				}
				started.add(e.getId());
			}
		});
		started.forEach((i) -> {
			this.queue.remove(i);
		});
	}

	private ScheduledTask createScheduledTask(Runnable r, long wait, long interval, boolean async) {
		long tid = count.getAndIncrement();
		ScheduledTask t = new ScheduledTask(
			this, tid, new TimerRunnable(tid, r), wait, interval, async
		);
		this.queue.put(tid, t);
		return t;
	}

	@Override
	public boolean stop(long id) {
		ScheduledTask task = this.running.remove(id);
		if (task == null) {
			return false;
		}
		if (!task.isAsync()) {
			return true;
		}
		ScheduledFuture future = this.runningAsyncTasks.remove(task);
		future.cancel(true);
		return true;
	}

	@Override
	public boolean cancel(long id) {
		ScheduledTask task = this.running.remove(id);
		if (task == null) {
			return false;
		}
		if (!task.isAsync()) {
			return true;
		}
		ScheduledFuture future = this.runningAsyncTasks.get(task);
		future.cancel(false);
		return true;
	}

	@Override
	public ScheduledTask runSync(Runnable runnable) throws Throwable {
		Assert.assertNotNull(runnable);
		return createScheduledTask(runnable, -1, -1, false);
	}

	@Override
	public ScheduledTask runSyncLater(Runnable runnable, long wait) throws Throwable {
		Assert.assertNotNull(runnable);
		Assert.assertTrue(wait > 0);
		return createScheduledTask(runnable, wait, -1, false);
	}

	@Override
	public ScheduledTask runSyncRepeating(Runnable runnable, long wait, long interval) throws Throwable {
		Assert.assertNotNull(runnable);
		Assert.assertTrue(wait > 0 && interval > 0);
		return createScheduledTask(runnable, wait, interval, false);
	}

	@Override
	public ScheduledTask runAsync(Runnable runnable) throws Throwable {
		Assert.assertNotNull(runnable);
		return createScheduledTask(runnable, -1, -1, true);
	}

	@Override
	public ScheduledTask runAsyncLater(Runnable runnable, long wait) throws Throwable {
		Assert.assertNotNull(runnable);
		Assert.assertTrue(wait > 0);
		return createScheduledTask(runnable, wait, -1, true);
	}

	@Override
	public ScheduledTask runAsyncRepeating(Runnable runnable, long wait, long interval) throws Throwable {
		Assert.assertNotNull(runnable);
		Assert.assertTrue(wait > 0 && interval > 0);
		return createScheduledTask(runnable, wait, interval, true);
	}

}
