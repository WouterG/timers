package net.wouto.timers.handler.impl;

import net.wouto.timers.handler.SyncTimerModel;
import net.wouto.timers.handler.TimerModel;
import net.wouto.timers.obj.ScheduledTask;
import net.wouto.timers.obj.TimerRunnable;
import net.wouto.timers.util.TimerThreadFactory;
import org.junit.Assert;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class SyncThreadPoolTimer implements SyncTimerModel {

	private Map<Long, ScheduledTask> queue = new ConcurrentHashMap<>();
	private Map<Long, ScheduledTask> running = new ConcurrentHashMap<>();
	private AtomicLong count;

	public SyncThreadPoolTimer() {
		this.count = new AtomicLong();
	}

	@Override
	public void tick() {
		List<Long> started = new ArrayList();
		this.queue.values().forEach((e) -> {
			if (e.shouldStart()) {
				if (e.isCancelled()) {
					started.add(e.getId());
					return;
				}
				if (!running.containsKey(e.getId())) {
					running.put(e.getId(), e);
				}
				e.getJob().run();
				e.ran();
				if (!e.isRepeat() || e.isCancelled()) {
					running.remove(e.getId());
				}
				started.add(e.getId());
			}
		});
		started.forEach((i) -> {
			this.queue.remove(i);
		});
	}

	private ScheduledTask createScheduledTask(Runnable r, long wait, long interval, boolean async) {
		long tid = count.getAndIncrement();
		ScheduledTask t = new ScheduledTask(
				this, tid, new TimerRunnable(tid, r), wait, interval, async
		);
		this.queue.put(tid, t);
		return t;
	}

	@Override
	public boolean stop(long id) {
		ScheduledTask task = this.running.remove(id);
		if (task == null) {
			return false;
		}
		return true;
	}

	@Override
	public boolean cancel(long id) {
		return this.stop(id);
	}

	@Override
	public ScheduledTask runSync(Runnable runnable) throws Throwable {
		Assert.assertNotNull(runnable);
		return createScheduledTask(runnable, -1, -1, false);
	}

	@Override
	public ScheduledTask runSyncLater(Runnable runnable, long wait) throws Throwable {
		Assert.assertNotNull(runnable);
		Assert.assertTrue(wait > 0);
		return createScheduledTask(runnable, wait, -1, false);
	}

	@Override
	public ScheduledTask runSyncRepeating(Runnable runnable, long wait, long interval) throws Throwable {
		Assert.assertNotNull(runnable);
		Assert.assertTrue(wait > 0 && interval > 0);
		return createScheduledTask(runnable, wait, interval, false);
	}

}
