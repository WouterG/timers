package net.wouto.timers.util;

import net.wouto.timers.handler.BaseTimerModel;
import net.wouto.timers.handler.SyncTimerModel;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class TimerThreadFactory implements ThreadFactory {

	private ThreadGroup group;
	private Class<? extends BaseTimerModel> base;
	private AtomicInteger c = new AtomicInteger();

	public TimerThreadFactory(Class<? extends BaseTimerModel> base) {
		this.group = new ThreadGroup("WoutosTimers-" + base.getSimpleName());
		this.base = base;
	}

	@Override
	public Thread newThread(Runnable r) {
		return new Thread(group, r, group.getName() + "-" + c.incrementAndGet());
	}

	public Class<? extends BaseTimerModel> getBase() {
		return this.base;
	}

}
